<h1>Teamfight Tierlist</h1>
<h3>Summary</h3>
Teamfight Tierlist uses historical data from Riot Games API about match history of players in high leagues to determine what items and comps are now the most viable. We can see best champions overall, champion synergies (which champion paird with another gets the best winrate)and best items for any champion.

This application was created as a final project for Databases course on University of Warsaw. 
