DROP TABLE IF EXISTS OneMatch;
DROP TABLE IF EXISTS Summoner;
DROP TABLE IF EXISTS MatchParticipant;
DROP TABLE IF EXISTS ChampionType;
DROP TABLE IF EXISTS ChampionInstance;
DROP TABLE IF EXISTS ItemType;
DROP TABLE IF EXISTS Trait;
DROP TABLE IF EXISTS ChampionTrait;
DROP TABLE IF EXISTS ChampionEquipment;

CREATE TABLE OneMatch (
	uid VARCHAR(64) PRIMARY KEY
);

CREATE TABLE Summoner (
	uid VARCHAR(128) PRIMARY KEY,
	nick VARCHAR(32) UNIQUE NOT NULL,
	league VARCHAR(32)
);

CREATE TABLE MatchParticipant (
	uid INT PRIMARY KEY AUTO_INCREMENT,
	place INT NOT NULL,
	summoner VARCHAR(128) NOT NULL REFERENCES Summoner,
	oneMatch VARCHAR(64) NOT NULL REFERENCES OneMatch,
	CONSTRAINT UniqueParticipant UNIQUE(summoner, oneMatch)
);

CREATE TABLE ChampionType (
	name VARCHAR(32) PRIMARY KEY,
	rarity INT NOT NULL
);

CREATE TABLE ChampionInstance (
	uid INT PRIMARY KEY AUTO_INCREMENT,
	tier INT NOT NULL,
	championType VARCHAR(32) NOT NULL REFERENCES ChampionType,
	owner INT NOT NULL REFERENCES MatchParticipant
);

CREATE TABLE Trait (
	name VARCHAR(16) PRIMARY KEY,
	traitType VARCHAR(16) NOT NULL
);

CREATE TABLE ItemType (
	id INT PRIMARY KEY,
	name VARCHAR(32) NOT NULL,
	trait VARCHAR(16) REFERENCES Trait
);

CREATE TABLE ChampionTrait (
	champion VARCHAR(32) NOT NULL REFERENCES ChampionType,
	trait VARCHAR(16) NOT NULL REFERENCES Trait,
	PRIMARY KEY(champion, trait)
);

CREATE TABLE ChampionEquipment (
	championInstance INT NOT NULL REFERENCES ChampionInstance,
	item INT NOT NULL REFERENCES ItemType,
	slot INT NOT NULL,
	PRIMARY KEY(championInstance, item, slot)
);


