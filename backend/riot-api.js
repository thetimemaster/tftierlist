require("./info");
const request = require("request");
const SmartQueue = require('smart-request-balancer');

const config = {
    rules: {
        individual: {
            rate: 20,
            limit: 1,
            priority: 1
        },
        group: {
            rate: 20,
            limit: 60,
            priority: 1
        },
        broadcast: {
            rate: 30,
            limit: 1,
            priority: 2
        }
    },
    overall: {
        rate: 30,
        limit: 1
    }
}


const queue = new SmartQueue(config);


let next = new Date().getTime();
function waitForApi(fun)
{

    let time = new Date().getTime();

    if (time > next) next = time;

    var diff = next - time;

    next += 1200;

    setTimeout(fun, diff);
}


global.Riot = (function(){

    let methods = {};

    methods.get_summoners = function (foreachFunction, limit = 1000000000) {
        waitForApi(function() {
            request("https://eun1.api.riotgames.com/tft/league/v1/grandmaster?api_key=" + Info.riot_key, {json: true}, (err, res, body) => {
                if (err) {
                    return console.log(err);
                }
                else {
                    if(res.statusCode === 200) {
                        body.entries.forEach((v, i) => {
                            if (i < limit)
                                foreachFunction(v.summonerId)
                        });
                    }
                    else {
                        console.log(res + " " + body);
                    }
                }
            });
        });
    }

    methods.summonerId_to_summonerData = function(summonerId, onConversionFunction) {
        waitForApi(function () {
            request("https://eun1.api.riotgames.com/tft/summoner/v1/summoners/" + summonerId + "?api_key=" + Info.riot_key, {json: true}, (err, res, body) => {
                if (err) {
                    return console.log(err);
                }
                else
                {
                    if(res.statusCode === 200) {
                        onConversionFunction(body);
                    }
                    else {
                        console.log(res + " " + body);
                    }
                }
            });
        });
    }

    methods.get_matches = function(puuid, matchMapFunction, count = 20) {
        waitForApi(function () {
            request("https://europe.api.riotgames.com/tft/match/v1/matches/by-puuid/" + puuid + "/ids?count=" + count + "&api_key=" + Info.riot_key, {json: true}, (err, res, body) => {
                if (err) {
                    return console.log(err);
                } else {
                    if (res.statusCode === 200) {
                        body.forEach(v => matchMapFunction(v));
                    }
                    else {
                        console.log(res + " " + body);
                    }
                }
            });
        });
    }

    methods.get_one_match = function (matchId, onGet) {
        waitForApi(function() {
            request("https://europe.api.riotgames.com/tft/match/v1/matches/" + matchId + "?api_key=" + Info.riot_key, {json: true}, (err, res, body) => {
                if (err)
                {
                    return console.log(err);
                }
                else
                {
                    if (res.statusCode === 200) {
                        onGet(body);
                    }
                    else {
                        console.log(res + " " + body);
                    }
                }
            });
        });
    }

    return methods;

})();



