require("../returns");
const util = require('util');


global.DatabaseSelect = (function(){

    let methods = {};

    methods.bestFor = function(conn, champion, response) {

        conn.query(`
            SELECT
              MAX(ItemType.name) item,
              AVG(place) place,
              SUM(MatchParticipant.place = 1) / COUNT(MatchParticipant.uid) winratio,
              COUNT(MatchParticipant.uid) sampleSize
            FROM
              ChampionInstance
              LEFT JOIN MatchParticipant ON ChampionInstance.owner = MatchParticipant.uid
              LEFT JOIN ChampionEquipment ON ChampionInstance.uid = ChampionEquipment.championInstance
              LEFT JOIN ItemType On item = ItemType.id
            WHERE
              ChampionInstance.championType = ${conn.escape(champion)}
              AND NOT ISNULL(ItemType.name)
            GROUP BY
              ChampionEquipment.item
            ORDER BY 
              AVG(place) ASC 
        `, function(err, resp) {

            if (err) {
                console.log(err);
                Returns.error(1, "SQL error, check console log for more info.", response);
            }
            else {
                Returns.ok_json(response, resp);
            }
        });
    };

    methods.bestOn = function(conn, item, response) {
        conn.query(`
            SELECT
              MAX(ChampionInstance.championType) champion,
              AVG(place) place,
              SUM(MatchParticipant.place = 1) / COUNT(MatchParticipant.uid) winratio,
              COUNT(MatchParticipant.uid) sampleSize
            FROM
              ChampionInstance
              LEFT JOIN MatchParticipant ON ChampionInstance.owner = MatchParticipant.uid
              LEFT JOIN ChampionEquipment ON ChampionInstance.uid = ChampionEquipment.championInstance
              LEFT JOIN ItemType On item = ItemType.id
            WHERE
              ItemType.name = ${conn.escape(item)}
            GROUP BY
              ChampionInstance.championType
            ORDER BY 
              AVG(place) ASC 
        `, function(err, resp) {

            if (err) {
                console.log(err);
                Returns.error(1, "SQL error, check console log for more info.", response);
            }
            else {
                Returns.ok_json(response, resp);
            }
        });
    };

    methods.synergy = function(conn, champion, response) {

        conn.query(`
          SELECT
            ChampSecondary.championType champion,
            AVG(place) place,
            SUM(MatchParticipant.place = 1) / COUNT(MatchParticipant.uid) winratio,
            COUNT(DISTINCT MatchParticipant.uid) sampleSize
          FROM
            ChampionInstance ChampPrimary
              LEFT JOIN ChampionInstance ChampSecondary ON ChampPrimary.owner = ChampSecondary.owner
              LEFT JOIN MatchParticipant ON ChampPrimary.owner = MatchParticipant.uid
          WHERE
            ChampPrimary.championType = ${conn.escape(champion)}
          GROUP BY
            ChampSecondary.championType
          ORDER BY
            place ASC
        `, function (err, resp) {

            if (err) {
                console.log(err);
                Returns.error(1, "SQL error, check console log for more info.", response);
            }
            else {
                Returns.ok_json(response, resp);
            }

        })

    };

    methods.staticChampionInfo = async function(conn, response) {

        conn.query(`SELECT * FROM ChampionType`, async function (err, resp)
        {
            if (err) {
                console.log(err);
                Returns.error(1, "SQL error, check console log for more info.", response);
            }
            else {

                for (const i in resp) {

                    let r = await util.promisify(conn.query).call(conn,`SELECT trait FROM ChampionTrait WHERE champion = ${conn.escape(resp[i].name)}`);
                    resp[i].traits = r.map(el => el.trait);
                }

                Returns.ok_json(response, resp);
            }
        });
    };

    methods.bestChampions = function(conn, response) {
        conn.query(`
            SELECT
              MAX(ChampionInstance.championType) champion,
              AVG(place) place,
              SUM(MatchParticipant.place = 1) / COUNT(MatchParticipant.uid) winratio,
              COUNT(MatchParticipant.uid) sampleSize
            FROM
              ChampionInstance
              LEFT JOIN MatchParticipant ON ChampionInstance.owner = MatchParticipant.uid
            GROUP BY
              ChampionInstance.championType
            ORDER BY 
              AVG(place) ASC 
        `, function(err, resp) {

            if (err) {
                console.log(err);
                Returns.error(1, "SQL error, check console log for more info.", response);
            }
            else {
                Returns.ok_json(response, resp);
            }
        });
    };

    methods.bestItems = function(conn, response) {

        conn.query(`
            SELECT
              MAX(ItemType.name) item,
              AVG(place) place,
              SUM(MatchParticipant.place = 1) / COUNT(MatchParticipant.uid) winratio,
              COUNT(MatchParticipant.uid) sampleSize
            FROM
              ChampionInstance
              LEFT JOIN MatchParticipant ON ChampionInstance.owner = MatchParticipant.uid
              LEFT JOIN ChampionEquipment ON ChampionInstance.uid = ChampionEquipment.championInstance
              LEFT JOIN ItemType On item = ItemType.id
            WHERE
              NOT ISNULL(ItemType.name)
            GROUP BY
              ChampionEquipment.item
            ORDER BY 
              AVG(place) ASC 
        `, function(err, resp) {

            if (err) {
                console.log(err);
                Returns.error(1, "SQL error, check console log for more info.", response);
            }
            else {
                Returns.ok_json(response, resp);
            }
        });
    };

    return methods;

})();