const mysql = require('mysql');

global.Connect = function (onConnect) {

    let conn = mysql.createConnection({
        host: Info.database_host,
        user: Info.database_user,
        password: Info.database_pass,
        database: Info.database_name
    });

    conn.connect(function(err) {
        if (err) throw err;
        onConnect(conn);
    });
};