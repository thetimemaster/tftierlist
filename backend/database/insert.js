const fs = require('fs');
const path = require('path');

global.DatabaseInsert = (function() {

    let methods = {};

    methods.try_insert_summoner = function (conn, summonerData) {
        conn.query(`INSERT INTO Summoner VALUES (
          ${conn.escape(summonerData.puuid)},
          ${conn.escape(summonerData.name)},
          ''
        )`, function (err, result) {

            if (err)
                console.log(err);
            else
                console.log(result);
        });
    };

    methods.try_insert_match = function(conn, matchId, matchData) {

        let participants = matchData.info.participants;

        conn.beginTransaction(function (err_trans) {

            conn.query(`INSERT INTO OneMatch VALUES (${conn.escape(matchId)})`, function (err, result) {

                if (err) {
                    conn.rollback();
                    console.log(err);
                }
                else
                {
                    for (let i in participants)
                    {
                        let participant = participants[i];

                        conn.query(`INSERT INTO MatchParticipant (place, summoner, oneMatch) VALUES (
                          ${conn.escape(participant.placement)}, 
                          ${conn.escape(participant.puuid)},
                          ${conn.escape(matchId)}
                        )`, function (err, result,) {

                            if (err) {
                                conn.rollback();
                                console.log(err);
                            }

                            let playerId = result.insertId;

                            for (let c in participant.units) {

                                let champion = participant.units[c];

                                conn.query(`INSERT INTO ChampionInstance (tier, championType, owner) VALUES (
                                  ${conn.escape(champion.tier)},
                                  ${conn.escape(champion.character_id)},
                                  ${conn.escape(playerId)}
                                )`, function(err, result) {

                                    if (err) {
                                        conn.rollback();
                                        console.log(err);
                                    }

                                    let championId = result.insertId;

                                    for (let j in champion.items) {

                                        conn.query(`INSERT INTO ChampionEquipment VALUES(
                                          ${conn.escape(championId)},
                                          ${conn.escape(champion.items[j])},
                                          ${conn.escape(j)}
                                        )`);

                                    }
                                });

                            }
                        });
                    }

                    conn.commit();
                }

            });
        });


    };

    methods.close = function () {

    };

    methods.load_static_data = function (conn) {

        let rawTraits = fs.readFileSync(path.resolve("./public/riot_data/traits.json"));
        let rawChampions = fs.readFileSync(path.resolve("./public/riot_data/champions.json"));
        let rawItems = fs.readFileSync(path.resolve("./public/riot_data/items.json"));

        let traits = JSON.parse(rawTraits);
        let champions = JSON.parse(rawChampions);
        let items = JSON.parse(rawItems);

        conn.beginTransaction(conn, function() {
            conn.query(`TRUNCATE TABLE ItemType`);
            conn.query(`TRUNCATE TABLE Trait`);
            conn.query(`TRUNCATE TABLE ChampionType`);
            conn.query(`TRUNCATE TABLE ChampionTrait`);

            for(let i in traits) {
                conn.query(`INSERT INTO Trait VALUES (
                  ${conn.escape(traits[i].name)},
                  ${conn.escape(traits[i].type)}
                )`, function(err, result){
                    if (err) {
                        conn.rollback();
                        console.log(err);
                    }
                });
            }

            for(let i in items) {
                let trait = '';
                if (items[i].hasOwnProperty("trait")) trait = items[i].trait;

                conn.query(`INSERT INTO ItemType VALUES (
                  ${conn.escape(items[i].id)},
                  ${conn.escape(items[i].name)},
                  ${conn.escape(items[i].trait)}
                )`, function(err, result){
                    if (err) {
                        conn.rollback();
                        console.log(err);
                    }
                });
            }

            for(let i in champions) {
                conn.query(`INSERT INTO ChampionType VALUES (
                  ${conn.escape("TFT2_" + champions[i].champion)},
                  ${conn.escape(champions[i].cost)}
                )`, function(err, result){
                    if (err) {
                        conn.rollback();
                        console.log(err);
                    }
                });

                let traits = champions[i].traits;

                for(let t in traits) {
                    conn.query(`INSERT INTO ChampionTrait VALUES (
                      ${conn.escape("TFT2_" + champions[i].champion)},
                      ${conn.escape(traits[t])}
                    )`, function (err, result) {
                        if (err) {
                            conn.rollback();
                            console.log(err);
                        }
                    });
                }
            }
        });
    };

    return methods;
})();