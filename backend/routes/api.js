require('../returns');
require("../riot-api");
require("../database/connect");
require("../database/insert");
require("../database/select");

let express = require('express');
let router = express.Router();

// Connect to database
Connect(function (conn) {

    console.log("Connected");

    /* GET API. */
    router.get('/', function(req, res) {
        Returns.ok_json(res, {'ok': 'ok'});
    });

    router.get('/new-batch', function(req, res){

        Riot.get_summoners(function(summonerId,) {

            console.log("Got summoner "+summonerId);

            Riot.summonerId_to_summonerData(summonerId, function(data) {

                console.log("Converted " + summonerId + " to " + data.puuid);

                DatabaseInsert.try_insert_summoner(conn, data);

                Riot.get_matches(data.puuid, function(matchId) {

                    console.log("Found match " + matchId);

                    Riot.get_one_match(matchId, function(matchDesc) {

                        DatabaseInsert.try_insert_match(conn, matchId, matchDesc);

                        console.log("Match " + matchId + "desc: " + matchDesc);

                    });

                }, 20);

            });
        }, 5);

        Returns.ok_json(res, {'status': 'ok'});
    });

    router.get('/reload-static-data', function(req, res){

        DatabaseInsert.load_static_data(conn);

        Returns.ok_json(res, {'status': 'ok'});
    });

    router.get('/best-for/:champion', function (req, res) {

        DatabaseSelect.bestFor(conn, req.params.champion, res);

    });

    router.get('/best-on/:item', function (req, res) {

        DatabaseSelect.bestOn(conn, req.params.item, res);

    });

    router.get('/synergy/:champion', function (req, res, next) {

        DatabaseSelect.synergy(conn, req.params.champion, res);

    });

    router.get('/champions', function (req, res, next) {

        DatabaseSelect.staticChampionInfo(conn, res);

    });

    router.get('/best-items', function (req, res, next) {

        DatabaseSelect.bestItems(conn, res);

    });

    router.get('/best-champions', function (req, res, next) {

        DatabaseSelect.bestChampions(conn, res);

    });

    console.log("Initialized router");

});

module.exports = router;
