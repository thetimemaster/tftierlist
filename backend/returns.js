global.Returns = (function() {
    let methods = {};

    methods.ok_json = function(res, json) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
        res.setHeader('Access-Control-Allow-Info', true); // If needed

        res.writeHead(200);

        let full = {}
        full.status = 0;
        full.data = json;

        res.write(JSON.stringify(full));
        res.end();
    }

    methods.error = function(statusCode, message, res){
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
        res.setHeader('Access-Control-Allow-Info', true); // If needed

        res.writeHead(200);

        let output = {}
        output.status = statusCode;
        output.message = message;

        res.write(JSON.stringify(output));
        res.end;
    }



    return methods;
})();

