import React from 'react';
import './components/championIcon';
import './css/style.css';
import {
    BrowserRouter as Router,
    Switch,
    Route, useParams,
} from "react-router-dom";
import ChampionPage from './pages/championPage';
import ItemPage from './pages/itemPage';
import MainPage from './pages/mainPage'

function App() {

    function ChampionPath() {
      let { championName } = useParams();
      return <ChampionPage championName={championName}/>
    }

    function ItemPath() {
        let { itemName } = useParams();
        return <ItemPage itemName={itemName}/>
    }

    function HomePath() {
        return <MainPage/>
    }


    return (
    <div className="App">
        <Router>
            <Switch>
                <Route path="/champion/:championName" component={ChampionPath}/>
                <Route path="/item/:itemName" component={ItemPath}/>
                <Route  component={HomePath}/>
            </Switch>
        </Router>
    </div>
  );
}

export default App;
