class LoLUtils {

    static jsonPromise = new Promise(function(resolve, reject) {

        fetch('http://localhost:3000/api/champions/')
            .then((response) => resolve(response.json()))

    });

    static sanitizeName(name) {
        name = name.replace('TFT2_', '');
        name = name.replace(/([A-Z])/g, ' $1').replace('Qiyana Woodland', 'Qiyana Mountain').replace('Wind', 'Cloud').trim();
        return name;
    }

    static sanitizeItemName(name) {
        name = name.replace(/([A-Z])/g, ' $1').replace('Qiyana Woodland', 'Qiyana Mountain').replace('Wind', 'Cloud').trim();
        return name;
    }



    static getRarity(name, resp) {

        this.jsonPromise
            .then((responseJson) => {
                if (responseJson.status === 0) {

                    responseJson.data.forEach(pck => {
                        if (pck.name === name) resp(pck.rarity);
                    })
                }
                else
                    console.error(responseJson.message);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    static getTraits(name, resp) {

        this.jsonPromise
            .then((responseJson) => {
                if (responseJson.status === 0) {
                    this.loaded = true;

                    responseJson.data.forEach(pck => {
                        if (pck.name === name) resp(pck.traits);
                    })
                }
                else
                    console.error(responseJson.message);
            })
            .catch((error) => {
                console.error(error);
            });
    }

}
export default LoLUtils;