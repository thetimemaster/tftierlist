class StyleUtils {

    static winrateColor(avgPlace) {

        if (avgPlace < 3) return "winrate-diamond";
        if (avgPlace < 3.5) return "winrate-platinum";
        if (avgPlace < 4) return "winrate-gold";
        if (avgPlace < 4.5) return "winrate-silver";
        if (avgPlace < 5) return "winrate-bronze";
        return "winrate-iron";
    }
}
export default StyleUtils;

