import React from 'react';
import ChampionIcon from "../components/championIcon";
import Header from "../components/header";
import Container from "../components/container";
import ChampionListing from "../components/championListing";
import ItemListing from "../components/itemListing";
import LoLUtils from "../helpers/lolUtils"
import { Scrollbars } from 'react-custom-scrollbars';
import TraitInfo from "../components/traitInfo";

class ChampionPage extends React.Component{

    getTraits() {
        LoLUtils.getTraits(this.props.championName, tr => {
            this.setState({traits: tr})
        });
    }

    getSynergyData() {
        fetch('http://localhost:3000/api/synergy/' + this.props.championName)
            .then((response) => response.json())
            .then((responseJson) => {

                if (responseJson.status === 0)
                    this.setState({
                        synergyJson: responseJson.data
                    });
                else
                    console.error(responseJson.message);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    getItemsData() {
        fetch('http://localhost:3000/api/best-for/' + this.props.championName)
            .then((response) => response.json())
            .then((responseJson) => {

                if (responseJson.status === 0)
                    this.setState({
                        itemsJson: responseJson.data
                    });
                else
                    console.error(responseJson.message);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    constructor(props) {
        super(props);

        this.getSynergyData = this.getSynergyData.bind(this);
        this.getSynergyData();

        this.getItemsData = this.getItemsData.bind(this);
        this.getItemsData();

        this.getTraits = this.getTraits.bind(this);
        this.getTraits();

        this.state = {
            synergyJson: [],
            itemsJson: [],
            traits: [],
            name: props.championName,
            tier: 1
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.championName !== this.props.championName) {
            this.setState({
                name: this.props.championName,
                itemsJson: [],
                synergyJson: [],
                traits: []
            })

            this.getSynergyData();
            this.getItemsData();
            this.getTraits();
        }
    }

    render()
    {

        return(
        <>
            <Header/>
            <Container>
                <ChampionIcon championName={this.state.name} size={"big"} classExt={"center-mobile"}/>
                <h1 className={"page-title"}>{LoLUtils.sanitizeName(this.state.name)}</h1>

                <div className={"trait-container"}>
                    {this.state.traits.map((trait, i) => <TraitInfo key={i} traitName={trait}/>)}
                </div>

                <div className={"half-width"} style={{height: "60vh"}}>
                    <h2>Synergies</h2>
                    <Scrollbars>
                        {
                            this.state.synergyJson.map((row, i) =>
                            {
                                return (<ChampionListing key={i} name={row.champion} place={row.place} cost={row.cost}
                                                         sample={row.sampleSize} winratio={row.winratio}/>)
                            }
                        )}
                    </Scrollbars>
                </div>
                <div className={"half-width"} style={{height: "60vh"}}>
                    <h2>Items</h2>
                    <Scrollbars>
                        {
                            this.state.itemsJson.map((row, i) =>
                                {
                                    return (<ItemListing key={i} name={row.item} place={row.place}
                                                         sample={row.sampleSize} winratio={row.winratio}/>)
                                }
                            )}
                    </Scrollbars>
                </div>
            </Container>
        </>
        );
    }
}
export default ChampionPage;