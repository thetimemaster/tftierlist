import React from 'react';
import ItemIcon from "../components/itemIcon";
import Header from "../components/header";
import Container from "../components/container";
import ChampionListing from "../components/championListing";
import ItemListing from "../components/itemListing";
import LoLUtils from "../helpers/lolUtils"
import { Scrollbars } from 'react-custom-scrollbars';

class MainPage extends React.Component{

    getChampionsData() {
        fetch('http://localhost:3000/api/best-champions')
            .then((response) => response.json())
            .then((responseJson) => {

                if (responseJson.status === 0)
                    this.setState({
                        championsJson: responseJson.data
                    });
                else
                    console.error(responseJson.message);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    getItemsData() {
        fetch('http://localhost:3000/api/best-items')
            .then((response) => response.json())
            .then((responseJson) => {

                if (responseJson.status === 0)
                    this.setState({
                        itemsJson: responseJson.data
                    });
                else
                    console.error(responseJson.message);
            })
            .catch((error) => {
                console.error(error);
            });
    }


    constructor(props) {
        super(props);

        this.getChampionsData = this.getChampionsData.bind(this);
        this.getChampionsData();

        this.getItemsData = this.getItemsData.bind(this);
        this.getItemsData();

        this.state = {
            championsJson: [],
            itemsJson: []
        }
    }

    render()
    {

        return(
            <>
                <Header/>
                <Container>
                    <h1 className={"page-title-no-icon page-title"}>{"Welcome Summoner"}</h1>

                    <div className={"half-width"} style={{height: "60vh"}}>
                        <h2>Best champions</h2>
                        <Scrollbars>
                            {
                                this.state.championsJson.map((row, i) =>
                                    {
                                        return (<ChampionListing key={i} name={row.champion} place={row.place} cost={row.cost}
                                                                 sample={row.sampleSize} winratio={row.winratio}/>)
                                    }
                                )}
                        </Scrollbars>
                    </div>

                    <div className={"half-width"} style={{height: "60vh"}}>
                        <h2>Best items</h2>
                        <Scrollbars>
                            {
                                this.state.itemsJson.map((row, i) =>
                                    {
                                        return (<ItemListing key={i} name={row.item} place={row.place} cost={row.cost}
                                                             sample={row.sampleSize} winratio={row.winratio}/>)
                                    }
                                )}
                        </Scrollbars>
                    </div>
                </Container>
            </>
        );
    }
}
export default MainPage;