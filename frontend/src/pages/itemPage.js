import React from 'react';
import ItemIcon from "../components/itemIcon";
import Header from "../components/header";
import Container from "../components/container";
import ChampionListing from "../components/championListing";
import LoLUtils from "../helpers/lolUtils"
import { Scrollbars } from 'react-custom-scrollbars';

class ItemPage extends React.Component{

    getChampionsData() {
        fetch('http://localhost:3000/api/best-on/' + this.props.itemName)
            .then((response) => response.json())
            .then((responseJson) => {

                if (responseJson.status === 0)
                    this.setState({
                        championsJson: responseJson.data
                    });
                else
                    console.error(responseJson.message);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    constructor(props) {
        super(props);

        this.getChampionsData = this.getChampionsData.bind(this);
        this.getChampionsData();

        this.state = {
            championsJson: [],
            name: props.itemName
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.itemName !== this.props.itemName) {
            this.setState({
                name: this.props.itemName,
                championsJson: [],
            })

            this.getChampionData();
        }
    }

    render()
    {

        return(
            <>
                <Header/>
                <Container>
                    <ItemIcon itemName={this.state.name} size={"big"} classExt={"center-mobile"}/>
                    <h1 className={"page-title"}>{LoLUtils.sanitizeItemName(this.state.name)}</h1>

                    <div className={"half-width"} style={{height: "60vh"}}>
                        <h2>Best on</h2>
                        <Scrollbars>
                            {
                                this.state.championsJson.map((row, i) =>
                                    {
                                        return (<ChampionListing key={i} name={row.champion} place={row.place} cost={row.cost}
                                                                 sample={row.sampleSize} winratio={row.winratio}/>)
                                    }
                                )}
                        </Scrollbars>
                    </div>
                </Container>
            </>
        );
    }
}
export default ItemPage;