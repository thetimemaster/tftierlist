import React from 'react';
import StyleUtils from "../helpers/styleUtils";
import LoLUtils from "../helpers/lolUtils";
import ChampionIcon from  "./championIcon"
import {Link} from "react-router-dom"


class ChampionListing extends React.Component{



    render()
    {
        let listingStyle = {
          marginBottom: "10px",
          borderRadius: "3px"
        };

        let name = this.props.name;

        return (
            <Link to={"/champion/" + name}>
                <div style={listingStyle} className={"listing " + StyleUtils.winrateColor(this.props.place)}>
                    <ChampionIcon championName={name} size={"small"} cost={this.props.cost}/>
                    <div className={"listing-name"}>{LoLUtils.sanitizeName(name)}</div>
                    <div className={"listing-place"}>{"Avg. place: " + this.props.place.toFixed(2)}</div>
                    <div className={"listing-winrate"}>{"Winrate: " + this.props.winratio.toFixed(2)}</div>
                    {this.props.sample < 10 ?
                        (<div className={"sample-waring sample-waring-" + (this.props.sample < 5 ? "severe" : "mild")}
                              title={"We checked only " + this.props.sample + " matches for this listing"}>!</div>):
                        (<div/>)
                    }
                </div>
            </Link>
        );
    }
}
export default ChampionListing;