import React from 'react';

class ItemIcon extends React.Component{

    render()
    {
        let style = {
            backgroundImage: "url(/icons/items/"+this.props.itemName.replace(/'/g, '').replace(/ /g, '').replace(/\./g, '')+".png)"
        };

        return (


            <div className={`${this.props.classExt} icon item-icon size-${this.props.size}`}>
                <div style={style}/>
                {/*<div className="icon-name">{LoLUtils.sanitizeItemName(this.props.itemName)}</div>*/}
            </div>
        );
    }
}
export default ItemIcon;