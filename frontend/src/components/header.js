import React from 'react';

class Header extends React.Component {


    render()
    {

        return(
            <header className="header-main">
                <a href="/" className="logo-link">
                    <img src="/logo.png" className="header-main-logo"/><h2>TFTierlist</h2>
                </a>
            </header>
        )
    }
}

export default Header;