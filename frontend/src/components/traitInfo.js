import React from 'react';

class TraitInfo extends React.Component{

    render()
    {
        return (



            <div className={"trait-wrapper"}>
                <img className={"trait-wrapper-icon"} src={"/icons/traits/"+this.props.traitName.toLowerCase()+".png"}/>
                <h4 className={"trait-wrapper-text"}>{this.props.traitName}</h4>
            </div>
        );
    }
}
export default TraitInfo;