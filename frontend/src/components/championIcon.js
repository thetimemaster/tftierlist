import React from 'react';
import LoLUtils from "../helpers/lolUtils";

class ChampionIcon extends React.Component{


    state =
        {
            tier: 1,
        };

    getRarity() {
        LoLUtils.getRarity(this.props.championName, r => {
            this.setState({tier: r})
        });
    }

    constructor(props) {
        super(props);
        this.getRarity = this.getRarity.bind(this);
        this.getRarity();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.championName != this.props.championName) {
            this.getRarity();
        }
    }

    render()
    {
        let style = {
            backgroundImage: "url(/icons/champions/"+this.props.championName.replace('Woodland', 'Mountain').replace('Wind', 'Cloud')+".png)"
        };

        return (


            <div className={`${this.props.classExt} icon champion-icon champion-tier-${this.state.tier} champion-${this.props.championName} size-${this.props.size}`}>
                <div style={style}/>
                {/*<div className="icon-name">{LoLUtils.sanitizeName(this.props.championName)}</div>*/}
            </div>
        );
    }
}
export default ChampionIcon;